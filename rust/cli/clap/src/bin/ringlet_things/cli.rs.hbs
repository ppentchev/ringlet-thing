// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Parse command-line options for things and stuff.

use std::io;
use std::str::FromStr;

use anyhow::{anyhow, bail, Context as _, Error as AnyError, Result};
use clap::error::ErrorKind as ClapErrorKind;
use clap::Parser as _;
use clap_derive::{Parser, Subcommand};
use tracing::Level;

use {{ pathname }}::defs::Config;

/// A variable name/value pair.
#[derive(Debug, Clone)]
struct VarValue {
    /// The variable name.
    name: String,

    /// The value specified.
    value: String,
}

impl FromStr for VarValue {
    type Err = AnyError;

    fn from_str(varvalue: &str) -> Result<Self> {
        varvalue.split_once('=').map_or_else(
            || Err(anyhow!("Not a 'variable=value' pair: {varvalue:?}")),
            |(name, value)| {
                Ok(Self {
                    name: name.to_owned(),
                    value: value.to_owned(),
                })
            },
        )
    }
}

impl From<VarValue> for (String, String) {
    fn from(pair: VarValue) -> Self {
        (pair.name, pair.value)
    }
}

/// What to do, what to do?
#[derive(Debug, Subcommand)]
enum CliCommand {
    /// Copy a directory tree, rename files, replace strings within them.
    Run {
        /// The project name to generate.
        #[clap(short, long, allow_hyphen_values(true))]
        name: String,
    },
}

/// Program name - short description.
#[derive(Debug, Parser)]
#[clap(version)]
struct Cli {
    /// Debug mode; display even more diagnostic output.
    #[clap(short, long)]
    debug: bool,

    /// Verbose operation; display diagnostic output.
    #[clap(short, long)]
    verbose: bool,

    /// What to do?
    #[clap(subcommand)]
    cmd: CliCommand,
}

/// What to do, what to do?
#[derive(Debug)]
pub enum Mode {
    /// Copy a directory tree, rename files, replace strings within them.
    Run(Config),

    /// Usage or version output; already handled by the options parser.
    Handled,
}

/// Initialize the logging subsystem provided by the `tracing` library.
fn setup_tracing(verbose: bool, debug: bool) -> Result<()> {
    let sub = tracing_subscriber::fmt()
        .without_time()
        .with_max_level(if debug {
            Level::TRACE
        } else if verbose {
            Level::DEBUG
        } else {
            Level::INFO
        })
        .with_writer(io::stderr)
        .finish();
    #[allow(clippy::absolute_paths)]
    tracing::subscriber::set_global_default(sub).context("Could not initialize the tracing logger")
}

/// Parse the command-line arguments, determine the mode of operation.
///
/// # Errors
///
/// Propagate command-line parsing errors.
pub fn try_parse() -> Result<Mode> {
    let args = match Cli::try_parse() {
        Ok(args) => args,
        Err(err)
            if matches!(
                err.kind(),
                ClapErrorKind::DisplayHelp | ClapErrorKind::DisplayVersion
            ) =>
        {
            err.print()
                .context("Could not display the help or version output")?;
            return Ok(Mode::Handled);
        }
        Err(err) if err.kind() == ClapErrorKind::DisplayHelpOnMissingArgumentOrSubcommand => {
            err.print()
                .context("Could not display the usage or version message")?;
            bail!("Invalid or missing command-line options");
        }
        Err(err) => return Err(err).context("Could not parse the command-line options"),
    };
    setup_tracing(args.verbose, args.debug)
        .context("Could not initialize the logging infrastructure")?;
    match args.cmd {
        CliCommand::Run { name } => Ok(Mode::Run(Config::new(name).with_verbose(args.verbose))),
    }
}

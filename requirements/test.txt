# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

pygments >= 2.7, < 3
pytest >= 8, < 9
test-stages >= 0.2, < 0.3
tox >= 4, < 5
tox-uv >= 1.4, < 2

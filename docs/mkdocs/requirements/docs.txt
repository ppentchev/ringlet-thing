# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

mkdocs >= 1.4.2, < 2
mkdocs-material >= 9.1.2, < 10
mkdocstrings >= 0.27, < 0.28
mkdocstrings-python >= 1, < 2

# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Make sure the rust/* templates work."""

from __future__ import annotations

import itertools
import os
import shlex
import subprocess  # noqa: S404
import typing

import pytest

from . import test_base as base


if typing.TYPE_CHECKING:
    import pathlib
    from typing import Final


PWC_SELF: Final = {
    "ch-1": ["1", "2", "3"],
    "ch-2": ["6", "8", "10"],
}

PWC_INPUT: Final = {
    "ch-1": ["10"],
    "ch-2": ["24"],
}


def _test_diag(prog: pathlib.Path) -> None:  # noqa: PLR0915
    """Test a program that may or may not output diagnostic information."""

    def run_program(prog_args: list[str]) -> None:
        """Run the built program."""
        subprocess.check_call([prog, *prog_args, "run", "--name", "something"])  # noqa: S603

    def run_stdout_no_stderr(prog_args: list[str]) -> None:
        """Run the built program, check for output on stdout and not on stderr."""
        res: Final = subprocess.run(  # noqa: S603
            [prog, *prog_args],
            capture_output=True,
            check=True,
            encoding="UTF-8",
        )
        assert res.stdout is not None, repr(res)
        assert res.stdout, repr(res)
        assert res.stdout is not None, repr(res)
        assert not res.stderr, repr(res)

    def run_fail_stderr_not_stdout(prog_args: list[str], *, not_present: str) -> None:
        """Run the built program, check for output on stderr and not on stdout."""
        res: Final = subprocess.run(  # noqa: S603
            [prog, *prog_args],
            capture_output=True,
            check=False,
            encoding="UTF-8",
        )
        assert res.returncode, repr(res)
        assert res.stdout is not None, repr(res)
        assert not res.stdout, repr(res)
        assert res.stdout is not None, repr(res)
        assert res.stderr, repr(res)

        # Make sure the error message does not include a specific string
        assert not_present not in res.stderr

    def run_validate(arg: str, *, debug: bool = False, verbose: bool = False) -> None:
        """Make sure the program's output matches the expectations."""
        debug_args = ["-d"] if debug else []
        verbose_args = ["-v"] if verbose else []
        res: Final = subprocess.run(  # noqa: S603
            [prog, *debug_args, *verbose_args, "run", "--name", arg],
            capture_output=True,
            check=False,
            encoding="UTF-8",
        )
        assert res.returncode == 0

        assert res.stdout is not None
        stdout_lines = res.stdout.splitlines()
        assert len(stdout_lines) == 1

        assert res.stderr is not None
        stderr_lines = res.stderr.splitlines()
        assert len(stderr_lines) == 2 + int(debug)
        assert " INFO" in stderr_lines[-2]
        if debug:
            assert arg in stderr_lines[0]

    print("Running the program itself")
    run_program([])

    print("Running the program itself in verbose mode")
    run_program(["-v"])

    print("Running the program itself in debug mode")
    run_program(["-d", "-v"])

    print("Running the program with `--help` only")
    run_stdout_no_stderr(["--help"])

    print("Running the program with `run --help`")
    run_stdout_no_stderr(["run", "--help"])

    print("Running the program with `--version`")
    run_stdout_no_stderr(["--version"])

    print("Making sure the program run with no arguments will fail")
    run_fail_stderr_not_stdout([], not_present="caused by")

    for arg, debug, verbose in itertools.product(
        ["Stuff", "oh, just the usual!", "-argh"],
        [False, True],
        [False, True],
    ):
        print(f"Checking the program's output, {arg=!r} {debug=} {verbose=}")
        run_validate(arg, debug=debug, verbose=verbose)


def _test_pwc(prog_name: str, prog: pathlib.Path) -> None:
    """Make sure `ch-1` adds numbers and `ch-2` multiplies them."""
    print(f"Running {prog_name} with its self-tests")
    env_no_stdin: Final = os.environ | {"PWC_FROM_STDIN": ""}
    lines: Final = subprocess.check_output([prog], encoding="UTF-8", env=env_no_stdin).splitlines()  # noqa: S603
    assert lines == PWC_SELF[prog_name]

    print(f"Running {prog_name} with some input")
    env_stdin: Final = os.environ | {"PWC_FROM_STDIN": "1"}
    res: Final = subprocess.run(  # noqa: S603
        [prog],
        check=True,
        encoding="UTF-8",
        env=env_stdin,
        input="1 2 3 4\n",
        stdout=subprocess.PIPE,
    )
    lines_stdin: Final = res.stdout.splitlines()
    assert lines_stdin == PWC_INPUT[prog_name]


@pytest.mark.parametrize(
    ("relpath", "args", "prog_names"),
    [
        ("rust/cli/clap", {"website_section": base.PROJECT_WEBSITE_SECTION}, [base.PROJECT_NAME]),
        ("rust/cli/pwc", {"pwc_id": 616}, ["ch-1", "ch-2"]),
    ],
)
def test_tox(relpath: str, args: dict[str, str], prog_names: list[str]) -> None:
    """Clean up the test directory, put the files there, run Tox."""
    print()

    is_pwc: Final = relpath.endswith("/pwc")

    with base.with_testdir(relpath=relpath, args=args) as testd:
        testd.prepare()

        testd.run_renlace()

        if prog_names[0] == base.PROJECT_NAME:
            print("Renaming the src/bin/ subdirectory (renlace will do that some sunny day)")
            (testd.path / "src/bin" / base.PROJECT_PATH).rename(
                testd.path / "src/bin" / base.PROJECT_NAME,
            )

        cargo: Final = os.environ.get("CARGO", "cargo")

        def run_cargo(cargo_args: list[str]) -> None:
            """Run `cargo` in the project directory."""
            print(f"Running `{cargo} {shlex.join(cargo_args)}`")
            subprocess.check_call([cargo, *cargo_args], cwd=testd.path, env=testd.env)  # noqa: S603

        run_cargo(["fmt", "--check"])
        run_cargo(["build"])
        run_cargo(["doc", "--no-deps"])
        run_cargo(["test"])

        print(f"Running `run-clippy.sh -c {cargo} -n`")
        subprocess.check_call(["./run-clippy.sh", "-c", cargo, "-n"], cwd=testd.path, env=testd.env)  # noqa: S603

        for prog_name, prog in ((name, testd.path / "target/debug" / name) for name in prog_names):
            if is_pwc:
                _test_pwc(prog_name, prog)
            else:
                _test_diag(prog)

        if not is_pwc:
            testd.run_tox()

        testd.rmdir()

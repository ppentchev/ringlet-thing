# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Make sure the docs/* templates work."""

from __future__ import annotations

import typing

import pytest

from . import test_base as base


if typing.TYPE_CHECKING:
    from typing import Final


@pytest.mark.parametrize(
    ("relpath", "args"),
    [("docs/mkdocs", {"website_section": base.PROJECT_WEBSITE_SECTION})],
)
def test_tox(relpath: str, args: dict[str, str]) -> None:
    """Clean up the test directory, put the files there, run Tox."""
    print()

    testd: Final = base.ProgDir(relpath=relpath, args=args)
    testd.prepare()

    testd.run_renlace()

    testd.run_tox()

    testd.rmdir()

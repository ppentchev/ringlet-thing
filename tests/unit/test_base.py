# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Common definitions for the `renlace` test suite."""

from __future__ import annotations

import contextlib
import dataclasses
import os
import pathlib
import shutil
import subprocess  # noqa: S404
import sys
import tempfile
import typing


if typing.TYPE_CHECKING:
    from collections.abc import Iterator
    from typing import Final


TOPDIR: Final = pathlib.Path(__file__).parent.parent.parent
"""The `renlace` top-level source directory."""

TEST_BASE: Final = TOPDIR / "test-playground"
"""The base directory for generating files and running tests."""

PROJECT_NAME: Final = "renlace-test"
"""The name of the project to generate."""

PROJECT_PATH: Final = "renlace_test"
"""The pathname that `renlace` should derive from the project name."""

PROJECT_WEBSITE_SECTION: Final = "test"
"""The website section to use for the Ringlet templates."""


# Well, we would like that one to be frozen, but `__post_init__()` would be weird...
@dataclasses.dataclass
class ProgDir:
    """A directory to prepare for running the a single renlace test."""

    relpath: str
    """The template path relative to the top-level `renlace` source directory."""

    args: dict[str, str]
    """Additional arguments used in the template."""

    path: pathlib.Path = dataclasses.field(init=False)
    """The path to the directory where the program will be tested."""

    template_path: pathlib.Path = dataclasses.field(init=False)
    """The full path to the template directory."""

    env: dict[str, str] = dataclasses.field(init=False)
    """The environment variables to use for running child processes."""

    test_base: pathlib.Path = TEST_BASE
    """The top-level directory to copy the files into."""

    def __post_init__(self) -> None:
        """Set up the paths and other properties."""
        self.path = self.test_base / self.relpath
        self.template_path = TOPDIR / self.relpath
        self.env = self.clean_env()

    @staticmethod
    def clean_env() -> dict[str, str]:
        """Remove Python-, Tox-, and venv-related variables from the process environment."""
        return {
            name: value
            for name, value in os.environ.items()
            if not name.startswith(("PYTEST", "PYTHON", "TOX")) and name != "VIRTUAL_ENV"
        }

    def rmdir(self) -> None:
        """Clean up the test directory."""
        print(f"Cleaning up {self.path} if it exists")
        if self.path.is_dir() and not self.path.is_symlink():
            shutil.rmtree(self.path)
        elif self.path.is_symlink() or self.path.exists():
            self.path.unlink()

    def prepare(self) -> None:
        """Prepare the files and directories for running `renlace`."""
        self.path.parent.mkdir(mode=0o755, exist_ok=True, parents=True)
        self.rmdir()

    def run_renlace(self) -> None:
        """Run `renlace` to generate the project."""
        print(f"Running renlace to create {self.path} from {self.template_path}")
        var_args: Final = [f"-a{name}={value}" for name, value in sorted(self.args.items())]
        subprocess.check_call(  # noqa: S603
            [  # noqa: S607
                "renlace",
                "run",
                *var_args,
                "--template",
                self.template_path,
                "--output-dir",
                self.path,
                "--name",
                PROJECT_NAME,
            ],
            env=self.env,
        )

        print("Initializing a new Git repository")
        subprocess.check_call(["git", "init"], cwd=self.path, env=self.env)  # noqa: S603,S607

        print("Running printenv")
        subprocess.check_call(["printenv"], env=self.env)  # noqa: S603,S607

    def run_tox(self, *, more_stages: list[str] | None = None) -> None:
        """Run Tox in the generated project directory."""
        print("Running Tox")
        subprocess.check_call(  # noqa: S603
            [sys.executable, "-m", "test_stages.tox_stages", "run"],
            cwd=self.path,
            env=self.env,
        )

        if more_stages is not None:
            print(f"Running additional Tox stages: {' '.join(more_stages)}")
            subprocess.check_call(  # noqa: S603
                [sys.executable, "-m", "test_stages.tox_stages", "run", "--", *more_stages],
                cwd=self.path,
                env=self.env,
            )


@contextlib.contextmanager
def with_testdir(
    relpath: str,
    args: dict[str, str],
    *,
    use_tempd: bool | None = None,
) -> Iterator[ProgDir]:
    """Either create a temporary directory or use the `test-playground` one."""
    if use_tempd is None:
        print("Getting use_tempd from the process environment...")
        use_tempd = os.environ.get("TEST_RENLACE_USE_TEMPDIR", "1") == "1"
        print(f"Got use_tempd {use_tempd}")

    if not use_tempd:
        yield ProgDir(relpath=relpath, args=args)
        return

    pwd: Final = pathlib.Path().cwd()
    with tempfile.TemporaryDirectory(prefix="test-renlace.") as tempd_obj:
        tempd: Final = pathlib.Path(tempd_obj)
        # Make sure the temporary directory is not within our source directory
        try:
            temprel: pathlib.Path | None = tempd.resolve().relative_to(pwd)
        except ValueError:
            temprel = None
        if temprel is not None:
            raise RuntimeError(repr((pwd, tempd, temprel)))

        yield ProgDir(relpath=relpath, args=args, test_base=tempd / "test-playground")

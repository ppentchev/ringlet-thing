# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Make sure the python/* templates work."""

from __future__ import annotations

import pytest

from . import test_base as base


@pytest.mark.parametrize(
    ("relpath", "args"),
    [("python/hatchling", {"website_section": base.PROJECT_WEBSITE_SECTION})],
)
def test_tox(relpath: str, args: dict[str, str]) -> None:
    """Clean up the test directory, put the files there, run Tox."""
    print()

    with base.with_testdir(relpath=relpath, args=args) as testd:
        testd.prepare()

        testd.run_renlace()

        testd.run_tox(more_stages=["pyupgrade"])

        testd.rmdir()
